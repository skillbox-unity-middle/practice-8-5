using UnityEngine;
using Zenject;

public class ZenjectInstaller : MonoInstaller
{
    [SerializeField] private GameBalance _balance;
    [SerializeField] private bool _isDummyClass = false;

    public override void InstallBindings()
    {
        if (!_isDummyClass) Container.Bind<ISettings>().FromInstance(_balance);
        else Container.Bind<ISettings>().To<DummyGameBalance>().AsSingle().NonLazy();
    }
}